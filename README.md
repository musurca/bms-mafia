# BMS Fighter Mafia
##### *Maximizing the OTHER jets in Falcon BMS!*

[**DOWNLOAD LATEST RELEASE**](https://gitlab.com/musurca/bms-mafia/-/archive/main/bms-mafia-main.zip)

*Pull requests welcome. Please join us on [Discord](https://discord.gg/pNFH4TzhPu) for questions or suggestions!*

This package contains fixes, improvements, and high-resolution skins for the following jets and vehicles in Falcon BMS 4.35:

* A7E
* AV8B & AV8B+
* CVNs
* EF-18M
* F-18C
* F-18E
* All F-18 variants
* Mirage 2000D
* Su-25

### How to Install:

Download the latest release, and unzip it to a temporary directory. Then do one of the following:

*Normal method:*

Open up the folder containing the modded Mafia plane you want, and copy the contents to the BMS theater in which you want to use it.

For example: to use the Mafia F-18E with the default Korea theater, open up the `F-18E` folder and copy its contents into your `<Falcon BMS Directory>/Data`, overwriting as necessary. (MAKE SURE YOU BACK UP FIRST!)

*[JSGME](https://www.falcon-lounge.com/falcon-bms-essentials/tutorials/install-mods-falcon-bms/) method (recommended):*

Drop the folder containing the plane you want to use into your `MODS` directory, then activate it as usual from within JSGME.

------

### CHANGELIST

#### A7E
* US Navy skin

#### AV8B & AV8B+
* Cockpit hotspots broken in 4.35 restored
* Kneeboards added

#### CVNs
* High-resolutions skin for carrier deck

#### EA-18G
* (optional) New model for plane + AN/ALQ-99 (Kaos)

#### EF-18M
* Work in progress
* (optional) TGP placement modified for better visibility

#### F-18C
* Cockpit hotspots broken in 4.35 restored

#### F-18E
* Cockpit hotspots broken in 4.35 restored
* HTS capacity added
* F404 engines swapped with more powerful (and accurate) F414
* Improvements to landing gear and brakes
* Low-visibility VFA-113 skin

#### F-18 Cockpit Fixes
* Cockpit needles fixed in night lighting for all F-18 variants

#### Mirage 2000D
* Cockpit hotspots broken in 4.35 restored

#### Su-25
* New cockpit from MiG-29
* Old school avionics

----

### ACKNOWLEDGEMENTS

The following people have contributed to the BMS Fighter Mafia:
* Airwolf
* Eddie
* Chuckles
* Dardo
* Kaos
* Ronin
* Gadfly
